## Install python requirements
``pip install -r requirements.txt``

## Hyper-parameters explanation
https://github.com/Unity-Technologies/ml-agents/blob/main/docs/Training-Configuration-File.md​

<hr>

## Start training
### With Unity Environment
``mlagents-learn configs\obstacleJump_curriculum_ppo.yaml --run-id=walljump_ppo``
### With prebuild environment
``mlagents-learn configs\obstacleJump_sac.yaml --run-id=walljump_sac --env=BUILD_LOCATION --num-envs=2 --resume``